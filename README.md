# exwhyscope

<https://mathr.co.uk/exwhyscope>

## what

A simple XY-scope using JACK for audio input and OpenGL for video output.

Now also an XY control surface: the mouse cursor position is output to JACK.
Note: loud, low frequency, with strong DC offsets: not for audio device.

## but

There are other software oscilloscopes supporting JACK, but I couldn't find
one with XY mode.

## get

Browse repository at <https://code.mathr.co.uk/exwhyscope>.

    git clone https://code.mathr.co.uk/exwhyscope.git

## build

    make

## run

    ./exwhyscope
    jack_connect system:capture_1 exwhyscope:in_x_1
    jack_connect system:capture_2 exwhyscope:in_y_1
    jack_connect exwhyscope:out_x synth:cv_in_1
    jack_connect exwhyscope:out_y synth:cv_in_2

## config

The window size, JACK client name, and input channels (8 XY pairs) are
hardcoded for now.  Edit the source code and recompile to change them.

## legal

Copyright (C) 2019,2023 Claude Heiland-Allen <claude@mathr.co.uk>

License GPL3+ <https://www.gnu.org/licenses/gpl.html>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- 
<https://mathr.co.uk>
