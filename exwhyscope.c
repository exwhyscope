/*
exwhyscope -- a simple XY oscilloscope using JACK and OpenGL
Copyright (C) 2019,2023 Claude Heiland-Allen <claude@mathr.co.uk>

License GPL3+ <https://www.gnu.org/licenses/gpl.html>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <assert.h>
#include <stdio.h>
#include <jack/jack.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// window size
#define WIDTH 512
#define HEIGHT 512

#define C 8
// must be multiple of JACK block size
#define N 65536
jack_client_t *client;
jack_port_t *in_port[C][2];
jack_port_t *out_port[2];
int ix = 0;
float xy[C][N][2];
volatile float mouse_x = 0;
volatile float mouse_y = 0;

int processcb(jack_nframes_t n, void *arg)
{
  (void) arg;
  if (ix + n <= N)
  {
    for (int c = 0; c < C; ++c)
    {
      jack_default_audio_sample_t *x = (jack_default_audio_sample_t *)
        jack_port_get_buffer(in_port[c][0], n);
      jack_default_audio_sample_t *y = (jack_default_audio_sample_t *)
        jack_port_get_buffer(in_port[c][1], n);
      for (jack_nframes_t i = 0; i < n; ++i)
      {
        xy[c][ix + i][0] = x[i];
        xy[c][ix + i][1] = y[i];
      }
    }
    float out_x = mouse_x;
    float out_y = mouse_y;
    jack_default_audio_sample_t *x = (jack_default_audio_sample_t *)
      jack_port_get_buffer(out_port[0], n);
    jack_default_audio_sample_t *y = (jack_default_audio_sample_t *)
      jack_port_get_buffer(out_port[1], n);
    for (jack_nframes_t i = 0; i < n; ++i)
    {
      x[i] = out_x;
      y[i] = out_y;
    }
  }
  ix += n;
  if (ix >= N) ix = 0;
  return 0;
}

int main()
{
  // start JACK
  if (! (client = jack_client_open("exwhyscope", 0, 0)))
  {
    fprintf(stderr, "jack server not running?\n");
    return 1;
  }
  jack_set_process_callback(client, processcb, 0);
  for (int c = 0; c < C; ++c)
  {
    char x[7] = { 'i', 'n', '_', '1' + c, '_', 'x', 0 };
    char y[7] = { 'i', 'n', '_', '1' + c, '_', 'y', 0 };
    in_port[c][0] = jack_port_register(client, x, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    in_port[c][1] = jack_port_register(client, y, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
  }
  out_port[0] = jack_port_register(client, "out_x", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  out_port[1] = jack_port_register(client, "out_y", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  if (jack_activate(client))
  {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }
  // start OpenGL
  glfwInit();
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_TRUE);
  glfwWindowHint(GLFW_FLOATING, GL_TRUE);
  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "exwhyscope", 0, 0);
  assert(window);
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  glClearColor(0, 0, 0, 0);
  glDisable(GL_DEPTH_TEST);
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, C * N * 2 * sizeof(GLfloat), xy, GL_STREAM_DRAW);
  glVertexPointer(2, GL_FLOAT, 0, 0);
  glEnableClientState(GL_VERTEX_ARRAY);
  // main loop
  while (1)
  {
    glfwPollEvents();
    if (glfwWindowShouldClose(window)) { break; }
    // handle mouse cursor to JACK output
    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);
    xpos /= WIDTH;
    ypos /= HEIGHT;
    xpos *= 2;
    ypos *= -2;
    xpos -= 1;
    ypos += 1;
    // small timing race if JACK callback runs between the next 2 lines
    mouse_x = xpos;
    mouse_y = ypos;
    // draw
    glClear(GL_COLOR_BUFFER_BIT);
    glBufferSubData(GL_ARRAY_BUFFER, 0, C * N * 2 * sizeof(GLfloat), xy);
    for (int c = 0; c < C; ++c)
    {
      const GLfloat colour[C][4] =
        { { 1, 0, 0, 1 }
        , { 0, 1, 0, 1 }
        , { 0, 0, 1, 1 }
        , { 1, 1, 0, 1 }
        , { 0, 1, 1, 1 }
        , { 1, 0, 1, 1 }
        , { 1, 1, 1, 1 }
        , { 0.5, 0.5, 0.5, 1 }
        };
      glColor4fv(&colour[c][0]);
      glDrawArrays(GL_POINTS, c * N, N);
    }
    glfwSwapBuffers(window);
    GLint e;
    while ((e = glGetError()))
      fprintf(stderr, "OpenGL ERROR %d\n", e);
  }
  // cleanup
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
