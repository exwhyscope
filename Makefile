all: exwhyscope

clean:
	-rm exwhyscope

exwhyscope: exwhyscope.c
	gcc -std=c99 -Wall -Wextra -pedantic -O3 -Ofast -march=native -ffast-math -o exwhyscope exwhyscope.c -ljack -lGL -lGLEW -lglfw
